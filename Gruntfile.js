module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // running `grunt uglify` will compile once
        uglify: {
            development: {
                options: {
                    preserveComments: false,
                    mangle: false,
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
                },
                files: {
                    './js/global.all.min.js': [
                        "./js/jquery.min.js",
                        "./js/bootstrap.min.js",
                        "./js/global.js"
                    ]
                }
            }
        },

        // running `grunt less` will compile once
        less: {
            development: {
                options: {
                    paths: ["./css"],
                    compress: true
                },
                files: {
                    "./css/global.all.min.css": [
                        "./css/bootstrap.css",
                        "./css/font-awesome.css",
                        "./less/global.less",
                        "./less/header.less",
                        "./less/footer.less",
                    ]
                }
            }
        },

        // running `grunt watch` will watch for changes
        watch: {
            scripts: {
                files: ['./js/*.js', '!./js/*.all.min.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['./less/*.less'],
                tasks: ['less']
            }
        }
    });

    // Load the plugin that provides the tasks.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s)
    grunt.registerTask('default', ['uglify', 'less']);
};